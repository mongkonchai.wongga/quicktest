/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from 'src/modules/auth/auth.module';
import { UsersModule } from 'src/modules/users/users.module';
import { TypegooseModule } from 'nestjs-typegoose';
import { SwaggerModule } from '@nestjs/swagger';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';

@Module({
  imports: [
    SwaggerModule,
    UsersModule,
    AuthModule,
    TypegooseModule.forRoot('mongodb+srv://superadmin:admin123456@clusterone.cllol0v.mongodb.net/?retryWrites=true&w=majority')
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard
    }
  ],
})
export class AppModule { }
