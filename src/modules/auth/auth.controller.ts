/* eslint-disable prettier/prettier */
import { Controller, Post, Body, Headers } from '@nestjs/common';
import { AuthService } from './auth.service';
import { loginDto } from './dto/login.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SkipAuth } from 'src/utils/decorators/skipAuth.decorators';

@ApiBearerAuth()
@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @SkipAuth()
  @Post('/login')
  login(@Body() req: loginDto) {
    return this.authService.login(req)
  }

  @Post('/profile')
  profile(@Headers('authorization') auth: string) {
    return this.authService.profile(auth)
  }

}
