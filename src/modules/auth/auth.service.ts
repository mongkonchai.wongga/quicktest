/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable prettier/prettier */
/* eslint-disable @typescript-eslint/no-empty-function */
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { Users } from 'src/models/user.model';
import { ModelType } from '@typegoose/typegoose/lib/types';
import * as bcrypt from 'bcrypt';
import { loginDto } from './dto/login.dto';
import { JwtService } from '@nestjs/jwt';


@Injectable()
export class AuthService {
  private data: any;
  constructor(
    @InjectModel(Users)
    private readonly _userModel: ModelType<Users>,
    private jwtService: JwtService,


  ) { }

  async login(req: loginDto) {

    const user = await this._userModel.findOne({ username: req.username }).exec()

    if (!user) throw new UnauthorizedException({ message: 'username or password not found on code: 0x001' })
    const comparePassword = await this.comparePassword(user.password, req.password);
    if (!comparePassword) throw new UnauthorizedException({ message: 'username or password not found on code: 0x002' })

    const payload = { userId: user.id, user: user.username }
    const accessToken = await this.jwtService.sign(payload);

    this.data = { accessToken }

    return this.data
  }

  async profile(auth: string) {
    if (!auth) throw new UnauthorizedException()
    const decode: any = await this.decode(auth);
    if (!decode) throw new UnauthorizedException()
    const user: any = await this._userModel.findOne({ _id: decode.userId }).exec()
    if (!user) throw new UnauthorizedException()
    const plainUser = user.toObject();
    delete plainUser.password;
    delete plainUser.createdAt
    delete plainUser.updatedAt
    delete plainUser.__v
    delete plainUser.country_id.__v

    return plainUser;
 
  }

  protected async decode(auth: string) {
    const jwt = auth.replace('Bearer ', '');
    return this.jwtService.decode(jwt, { json: true });
  }

  protected async comparePassword(userPassword: string, password: string): Promise<any> {
    return await bcrypt.compare(password, userPassword);
  }
}
