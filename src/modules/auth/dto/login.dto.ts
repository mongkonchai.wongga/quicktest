/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString } from "class-validator";

export class loginDto {

    @ApiProperty({ example: '' })
    @IsNotEmpty()
    @IsString()
    readonly username: string

    @ApiProperty({ example: '' })
    @IsNotEmpty()
    @IsString()
    readonly password: string

}
