/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { Users } from 'src/models/user.model';
import { TypegooseModule } from 'nestjs-typegoose';
import { Country } from 'src/models/country.model';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [
    TypegooseModule.forFeature([Users, Country]), AuthModule
  ],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule { }
