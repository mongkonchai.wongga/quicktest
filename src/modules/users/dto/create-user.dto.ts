/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateUserDto {
    @ApiProperty({ example: 'quicktest'})
    @IsNotEmpty()
    readonly username: string

    @ApiProperty({ example: 'password'})
    @IsNotEmpty()
    readonly password: string

    @ApiProperty({ example: 'Harry'})
    @IsNotEmpty()
    readonly first_name: string

    @ApiProperty({ example: 'Potter'})
    @IsNotEmpty()
    readonly last_name: string

    @ApiProperty({ example: '6494d4279a0b93955a2e3dd4'})
    @IsNotEmpty()
    readonly country_id: string
}
