/* eslint-disable prettier/prettier */
import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";

export class UpdateUserDto {

    @ApiProperty({ example: '' })
    @IsOptional()
    readonly username: string

    @ApiProperty({ example: '' })
    @IsOptional()
    readonly password: string

    @ApiProperty({ example: '' })
    @IsOptional()
    readonly first_name: string

    @ApiProperty({ example: '' })
    @IsOptional()
    readonly last_name: string

    @ApiProperty({ example: ''})
    @IsOptional()
    readonly country_id: string

}
