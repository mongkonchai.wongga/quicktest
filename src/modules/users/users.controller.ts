/* eslint-disable prettier/prettier */
import { Controller, Get, Post, Body, Param, Delete, Put, Headers, Patch } from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { SkipAuth } from 'src/utils/decorators/skipAuth.decorators';

@ApiBearerAuth()
@Controller('users')
@ApiTags('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @SkipAuth()
  @Post('register')
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }

  @SkipAuth()
  @Post('/create-country')
  createCountry() {
    return this.usersService.createCountry();
  }

  @Get()
  findAll() {
    return this.usersService.findAll();
  }

  @Get('/country')
  findAllCountry() {
    return this.usersService.findAllCountry();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.usersService.findOne(id);
  }

  @Put('/update')
  update(
    @Headers('authorization') auth: string,
    @Body() updateUserDto: UpdateUserDto) {
    return this.usersService.update(auth, updateUserDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.usersService.remove(id);
  }
}
