/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable prettier/prettier */
import { BadRequestException, Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from 'nestjs-typegoose';
import { Users } from 'src/models/user.model';
import { ModelType, ReturnModelType } from '@typegoose/typegoose/lib/types';
import * as bcrypt from 'bcrypt';
import { Country } from 'src/models/country.model';
import { AuthService } from '../auth/auth.service';
const fs = require('fs');


@Injectable()
export class UsersService {

  protected readonly saltRounds = 10;
  protected readonly masterCountry = 'src/config/master_country.json';

  constructor(
    @InjectModel(Users)
    private readonly _userModel: ReturnModelType<typeof Users>,

    @InjectModel(Country)
    private readonly _countryModel: ReturnModelType<typeof Country>,

    private _authService: AuthService,

  ) { }

  async findAll() {
    const find = await this._userModel.find().populate('country_id').exec();

    const usersWithoutPassword = find.map(user => {
      const { password, ...userWithoutPassword } = user.toObject();
      return userWithoutPassword;
    });

    return usersWithoutPassword;
  }

  async findOne(id: string) {
    const find = await this._userModel.findOne({ _id: id }).populate('country_id').exec();
    if (!find) throw new NotFoundException()

    const { password, ...userWithoutPassword } = find.toObject();

    return userWithoutPassword;
  }

  async findAllCountry() {
    const find = await this._countryModel.find().exec();
    return find;
  }

  async create(req: CreateUserDto) {
    const findUsername = await this._userModel.count({ username: req.username }).exec();
    if (findUsername > 0) throw new BadRequestException({ msg: "username is Duplicate" });
    const hash = await bcrypt.hash(req.password, this.saltRounds)

    const dataStore = {
      ...req,
      password: hash
    }

    try {
      await this._userModel.insertMany(dataStore);
      return true
    } catch (error) {
      throw new InternalServerErrorException()
    }
  }

  async update(auth: string, req: UpdateUserDto) {

    const me = await this._authService.profile(auth)
    console.log(me);

    let dataUpdate = {
      ...req,
    }

    if (req?.password) {
      dataUpdate.password = await bcrypt.hash(req.password, this.saltRounds);
    }

    if (req?.username) {
      const findUsername = await this._userModel.findOne({ username: req.username, _id: { $ne: me._id } }).exec();
      if (findUsername) throw new BadRequestException({ msg: "username is Duplicate" });
      dataUpdate.username = req.username
    }

    try {
      const result = await this._userModel.updateOne({ _id: me._id }, dataUpdate);
      return result
    } catch (error) {
      throw new InternalServerErrorException()
    }
  }

  async createCountry() {
    const data = fs.readFileSync(this.masterCountry, 'utf8');
    const jsonParse = JSON.parse(data);

    try {
      const result = await this._countryModel.insertMany(jsonParse);
      return result
    } catch (error) {
      throw new InternalServerErrorException()
    }
  }

  async remove(id: string) {
    const item = await this._userModel.findOne({ _id: id }).exec();
    if (!item) throw new BadRequestException({ message: new NotFoundException(id) });

    try {
      const result = await this._userModel.deleteOne({ _id: id });
      return result
    } catch (error) {
      throw new InternalServerErrorException()
    }

  }
}
