/* eslint-disable prettier/prettier */
import { Ref, modelOptions, plugin, prop } from '@typegoose/typegoose';
import { Country } from './country.model';
import * as autopopulate from 'mongoose-autopopulate';

@modelOptions({
    schemaOptions: {
        timestamps: true,
        // toJSON: { virtuals: true },
        // toObject: { virtuals: true },
    }
})

@plugin(autopopulate as any)
export class Users {
    @prop({ required: true, unique: true, type: () => String })
    public username: string;

    @prop({ required: true, type: () => String })
    public password: string;

    @prop({ required: true, type: () => String })
    public first_name: string;

    @prop({ required: true, type: () => String })
    public last_name: string;

    @prop({ autopopulate: true, ref: () => Country, type: () => String })
    public country_id: Ref<Country>;
}
