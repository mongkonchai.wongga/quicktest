/* eslint-disable prettier/prettier */
import { prop } from "@typegoose/typegoose";

export class Country {

    @prop({ type: () => String })
    public code: string

    @prop({ type: () => String })
    public name: string

}